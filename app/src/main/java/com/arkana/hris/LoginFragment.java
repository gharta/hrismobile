package com.arkana.hris;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 06/11/17.
 */

public class LoginFragment extends Fragment {
    String baseUrl;
    String sessionUrl;
    TextView login;
    TextView password;
    TextView ipAddress;
    Button loginButton;
    Button resetPassword;
    Button backLogin;
    TextView forgetPassword;
    TextView emailForgetPassword;
    TextInputLayout loginLayout;
    TextInputLayout passwordLayout;
    TextInputLayout emailForgetPasswordLayout;
//    String role;


    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (getActivity() != null && getActivity() instanceof MainActivity) {
            baseUrl = ((MainActivity) getActivity()).getBaseUrl();
            ((MainActivity) getActivity()).setActionBarTitle("HRIS");
            ((MainActivity) getActivity()).hideToolbar();
        }
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_login, container, false);

        login = view.findViewById(R.id.login);
        password = view.findViewById(R.id.password);
        loginButton = view.findViewById(R.id.loginButton);
        resetPassword = view.findViewById(R.id.resetPassword);
        forgetPassword = view.findViewById(R.id.forgetPassword);
        backLogin = view.findViewById(R.id.backLogin);
        emailForgetPassword = view.findViewById(R.id.emailForgetPassword);
        loginLayout = view.findViewById(R.id.loginLayout);
        passwordLayout = view.findViewById(R.id.passwordLayout);
        emailForgetPasswordLayout = view.findViewById(R.id.emailForgetPasswordLayout);
//        ipAddress = view.findViewById(R.id.ipAddress);
//        ipAddress.setText(baseUrl);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login(getContext(), baseUrl+"/api/login");
            }
        });
        resetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetPassword(getContext(), baseUrl+"/api/resetpassword");
                emailForgetPasswordLayout.setVisibility(View.GONE);
                resetPassword.setVisibility(View.GONE);
                backLogin.setVisibility(View.GONE);
                loginButton.setVisibility(View.VISIBLE);
                loginLayout.setVisibility(View.VISIBLE);
                passwordLayout.setVisibility(View.VISIBLE);
                forgetPassword.setVisibility(View.VISIBLE);
            }
        });
        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailForgetPasswordLayout.setVisibility(View.VISIBLE);
                resetPassword.setVisibility(View.VISIBLE);
                backLogin.setVisibility(View.VISIBLE);
                loginButton.setVisibility(View.GONE);
                loginLayout.setVisibility(View.GONE);
                passwordLayout.setVisibility(View.GONE);
                forgetPassword.setVisibility(View.GONE);
            }
        });
        backLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailForgetPasswordLayout.setVisibility(View.GONE);
                resetPassword.setVisibility(View.GONE);
                backLogin.setVisibility(View.GONE);
                loginButton.setVisibility(View.VISIBLE);
                loginLayout.setVisibility(View.VISIBLE);
                passwordLayout.setVisibility(View.VISIBLE);
                forgetPassword.setVisibility(View.VISIBLE);
            }
        });


        try {
            Activity activity = getActivity();
            if (activity instanceof MainActivity) {
                PackageInfo pInfo = ((MainActivity) activity).getPackageManager().getPackageInfo(((MainActivity) activity).getPackageName(), 0);
                String versionName = "Version " + pInfo.versionName;
                TextView appVersion = view.findViewById(R.id.appVersion);
                appVersion.setText(versionName);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

//        getOneData(context, baseUrl+"/partners/");
//        login(getContext(), baseUrl+"/api/login");
        return view;
    }

    private void login(Context context, String url) {
        if (baseUrl != null && !baseUrl.isEmpty())
            if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).setBaseUrl(baseUrl);
        else
            if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showSnackBar("Invalid IP Address");
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
        JSONObject jsonRequestBody = new JSONObject();
        try {
            jsonRequestBody.put("login", login.getText());
            jsonRequestBody.put("password", password.getText());
//            jsonRequestBody.put("role", role);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showLoading();
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url,
                jsonRequestBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).hideLoading();
                        // Display the first 500 characters of the response string.
                        try {
                            Activity activity = getActivity();
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("SESSION_ID", response.getString("session_id"));

                            if (activity != null && activity instanceof MainActivity)((MainActivity) activity).setSessionId(response.getString("session_id"));
                            if (response.has("role")) {
                                ((MainActivity) activity).setNavigation(response.getString("role"));
                                ((MainActivity) activity).setRole(response.getString("role"));
                                editor.putString("role", response.getString("role"));
                            }
                            if (response.has("employee_id")) {
                                if (activity instanceof MainActivity) ((MainActivity) activity).setEmployeeId(response.getLong("employee_id"));
                                editor.putLong("employee_id", response.getLong("employee_id"));
                            }
                            if (response.has("name") && response.has("username")) {
                                if (activity instanceof MainActivity) ((MainActivity) activity).setName(response.getString("name"));
                                if (activity instanceof MainActivity) ((MainActivity) activity).setUsername(response.getString("username"));
                                editor.putString("name", response.getString("name"));
                                editor.putString("username", response.getString("username"));
                            }
                            if (response.has("payslip_url") && response.has("overtime_url")) {
                                if (activity instanceof MainActivity) ((MainActivity) activity).setPayslipURL(response.getString("payslip_url"));
                                if (activity instanceof MainActivity) ((MainActivity) activity).setOvertimeURL(response.getString("overtime_url"));
                                editor.putString("payslip_url", response.getString("payslip_url"));
                                editor.putString("overtime_url", response.getString("overtime_url"));
                            }
                            if (activity instanceof MainActivity) ((MainActivity) activity).showToolbar();
                            if (activity instanceof MainActivity) ((MainActivity) activity).unlockDrawer();

                            sessionUrl = "?session_id="+response.getString("session_id");
                            if (activity instanceof MainActivity) ((MainActivity) activity).setSessionUrl(sessionUrl);

                            editor.commit();

                            Fragment fragment = ListOvertimeFragment.newInstance("actual_approved,cancel");
                            if (activity instanceof MainActivity) ((MainActivity) activity).replaceFragment(fragment);

                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");


                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = "";
                String msg = "";
                try {
                    body = new String(error.networkResponse.data,"UTF-8");
                    JSONObject jsonBody = new JSONObject(body);
                    msg = jsonBody.getString("error_descrip");
                    msg = msg.split("DETAIL")[0];
                    msg = msg.replace("('", "");
                    msg = msg.replace("', None)", "");
                } catch (UnsupportedEncodingException e) {
                    // exception
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).hideLoading();
                if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showSnackBar("Login Failed. " + msg);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                return params;
            }
        };
        queue.add(req);
    }

    private void resetPassword(Context context, String url) {
        if (baseUrl != null && !baseUrl.isEmpty())
            if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).setBaseUrl(baseUrl);
            else
            if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showSnackBar("Invalid IP Address");
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
        JSONObject jsonRequestBody = new JSONObject();
        try {
            jsonRequestBody.put("email", emailForgetPassword.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showLoading();
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url,
                jsonRequestBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).hideLoading();
                        // Display the first 500 characters of the response string.
                        try {
                            if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showSnackBar("Reset Password email has been sent to your email address");
                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");


                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = "";
                String msg = "";
                try {
                    body = new String(error.networkResponse.data,"UTF-8");
                    JSONObject jsonBody = new JSONObject(body);
                    msg = jsonBody.getString("error_descrip");
                    msg = msg.split("DETAIL")[0];
                    msg = msg.replace("('", "");
                    msg = msg.replace("', None)", "");
                } catch (UnsupportedEncodingException e) {
                    // exception
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).hideLoading();
                if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showSnackBar(msg);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                return params;
            }
        };
        queue.add(req);
    }
}