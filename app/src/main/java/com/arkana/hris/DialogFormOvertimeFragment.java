package com.arkana.hris;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * Dialog display "Thank you" screen after order is finished.
 */
public class DialogFormOvertimeFragment extends DialogFragment {

    private long overtimeId = 0;
    private JSONObject overtimeObject;
    private ObjectInterface dialogInterface;
    TextView tvTitle;
    EditText etStartDate;
    EditText etStartTime;
    EditText etEndTime;
    EditText etNotes;
    EditText etActualStartDate;
    EditText etActualStartTime;
    EditText etActualEndTime;
    EditText etActualNotes;
    LinearLayout actualWrapper;
    String valStartDate;
    String valStartTime;
    String valEndTime;
    String valActualStartDate;
    String valActualStartTime;
    String valActualEndTime;

    String baseUrl;
    String sessionUrl;
    String state;
    String role = "";
    String mode = "";

    /**
     * Dialog display "Thank you" screen after order is finished.
     */
    public static DialogFormOvertimeFragment newInstance(long overtimeId, JSONObject overtimeObject, String state, ObjectInterface dialogInterface) {
        DialogFormOvertimeFragment dialogFragment = new DialogFormOvertimeFragment();
        dialogFragment.overtimeId = overtimeId;
        dialogFragment.overtimeObject = overtimeObject;
        dialogFragment.dialogInterface = dialogInterface;
        dialogFragment.state = state;
        return dialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setWindowAnimations(R.style.dialogFragmentAnimation);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_form_overtime, container, false);
        if (getActivity() != null && getActivity() instanceof MainActivity) {
            baseUrl = ((MainActivity) getActivity()).getBaseUrl();
            sessionUrl = ((MainActivity) getActivity()).getSessionUrl();
            ((MainActivity) getActivity()).setActionBarTitle("Overtime");
            role = ((MainActivity) getActivity()).getRole();
            mode = ((MainActivity) getActivity()).getMode();
        }
        if (sessionUrl.isEmpty()) {
            Activity activity = getActivity();
            Fragment fragment = LoginFragment.newInstance();
            if (activity instanceof MainActivity) ((MainActivity) activity).replaceFragment(fragment);
        }

        final Button okBtn = (Button) view.findViewById(R.id.btn_ok);
        Button cancelBtn = (Button) view.findViewById(R.id.btn_cancel);

        tvTitle = view.findViewById(R.id.tvTitle);
        etStartDate = view.findViewById(R.id.etStartDate);
        etStartTime = view.findViewById(R.id.etStartTime);
        etEndTime = view.findViewById(R.id.etEndTime);
        etNotes = view.findViewById(R.id.etNotes);
        etActualStartDate = view.findViewById(R.id.etActualStartDate);
        etActualStartTime = view.findViewById(R.id.etActualStartTime);
        etActualEndTime = view.findViewById(R.id.etActualEndTime);
        etActualNotes = view.findViewById(R.id.etActualNotes);
        actualWrapper = view.findViewById(R.id.actualWrapper);

        tvTitle.setText("Overtime");
        etStartDate.setFocusable(false);
        etStartTime.setFocusable(false);
        etEndTime.setFocusable(false);
        etActualStartDate.setFocusable(false);
        etActualStartTime.setFocusable(false);
        etActualEndTime.setFocusable(false);
        if (!mode.equals("report") && role != null) {// && role.equals("employee")) {
            etNotes.setFocusable(true);
            etActualNotes.setFocusable(true);
            etNotes.setEnabled(true);
            etActualNotes.setEnabled(true);
            //Time
            if (state.equals("plan_draft") || state == null || state.isEmpty() || state.equals("false") || (state.equals("plan_waiting") && role.equals("manager"))) {
                etStartDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        SelectDateFragment selectDateFragment = SelectDateFragment.newInstance(new DateInterface() {
                            @Override
                            public void onDateSelected(View v, int yy, int mm, int dd) {
                                String smm = mm < 10 ? smm = "0"+mm : Integer.toString(mm);
                                String sdd = dd < 10 ? sdd = "0"+dd : Integer.toString(dd);
                                valStartDate = yy+"-"+smm+"-"+sdd;
                                etStartDate.setText(valStartDate);
                            }
                        });
                        selectDateFragment.show(getFragmentManager(), "DatePicker");
                    }
                });
                etStartTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        SelectTimeFragment selectTimeFragment = SelectTimeFragment.newInstance(new TimeInterface() {
                            @Override
                            public void onTimeSelected(View view, int hour, int minute) {
                                String stringHour = hour < 10 ? stringHour = "0" + hour : Integer.toString(hour);
                                String stringMinute = minute < 10 ? stringMinute = "0" + minute : Integer.toString(minute);
                                valStartTime = stringHour + ":" + stringMinute + ":00";
                                etStartTime.setText(stringHour + ":" + stringMinute);
                            }
                        });
                        selectTimeFragment.show(getFragmentManager(), "TimePicker");
                    }
                });
                etEndTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        SelectTimeFragment selectTimeFragment = SelectTimeFragment.newInstance(new TimeInterface() {
                            @Override
                            public void onTimeSelected(View view, int hour, int minute) {
                                String stringHour = hour < 10 ? stringHour = "0" + hour : Integer.toString(hour);
                                String stringMinute = minute < 10 ? stringMinute = "0" + minute : Integer.toString(minute);
                                valEndTime = stringHour + ":" + stringMinute + ":00";
                                etEndTime.setText(stringHour + ":" + stringMinute);
                            }
                        });
                        selectTimeFragment.show(getFragmentManager(), "TimePicker");
                    }
                });
            } else {
                okBtn.setVisibility(View.GONE);
                etStartDate.setBackgroundResource(android.R.color.transparent);
                etStartTime.setBackgroundResource(android.R.color.transparent);
                etEndTime.setBackgroundResource(android.R.color.transparent);
                etNotes.setBackgroundResource(android.R.color.transparent);
                etNotes.setFocusable(false);
            }
            if (state.equals("plan_approved") || (state.equals("actual_waiting") && role.equals("manager"))) {
                okBtn.setVisibility(View.VISIBLE);
                etActualStartDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        SelectDateFragment selectDateFragment = SelectDateFragment.newInstance(new DateInterface() {
                            @Override
                            public void onDateSelected(View v, int yy, int mm, int dd) {
                                String smm = mm < 10 ? smm = "0"+mm : Integer.toString(mm);
                                String sdd = dd < 10 ? sdd = "0"+dd : Integer.toString(dd);
                                valActualStartDate = yy+"-"+smm+"-"+sdd;
                                etActualStartDate.setText(valActualStartDate);
                            }
                        });
                        selectDateFragment.show(getFragmentManager(), "DatePicker");
                    }
                });
                etActualStartTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        SelectTimeFragment selectTimeFragment = SelectTimeFragment.newInstance(new TimeInterface() {
                            @Override
                            public void onTimeSelected(View view, int hour, int minute) {
                                String stringHour = hour < 10 ? stringHour = "0" + hour : Integer.toString(hour);
                                String stringMinute = minute < 10 ? stringMinute = "0" + minute : Integer.toString(minute);
                                valActualStartTime = stringHour + ":" + stringMinute + ":00";
                                etActualStartTime.setText(stringHour + ":" + stringMinute);
                            }
                        });
                        selectTimeFragment.show(getFragmentManager(), "TimePicker");
                    }
                });
                etActualEndTime.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        SelectTimeFragment selectTimeFragment = SelectTimeFragment.newInstance(new TimeInterface() {
                            @Override
                            public void onTimeSelected(View view, int hour, int minute) {
                                String stringHour = hour < 10 ? stringHour = "0" + hour : Integer.toString(hour);
                                String stringMinute = minute < 10 ? stringMinute = "0" + minute : Integer.toString(minute);
                                valActualEndTime = stringHour + ":" + stringMinute + ":00";
                                etActualEndTime.setText(stringHour + ":" + stringMinute);
                            }
                        });
                        selectTimeFragment.show(getFragmentManager(), "TimePicker");
                    }
                });
            } else {
                etActualStartTime.setBackgroundResource(android.R.color.transparent);
                etActualEndTime.setBackgroundResource(android.R.color.transparent);
                etActualNotes.setBackgroundResource(android.R.color.transparent);
                etActualNotes.setFocusable(false);
            }
        } else {
            okBtn.setVisibility(View.GONE);
            etStartDate.setBackgroundResource(android.R.color.transparent);
            etActualStartDate.setBackgroundResource(android.R.color.transparent);
            etStartTime.setBackgroundResource(android.R.color.transparent);
            etEndTime.setBackgroundResource(android.R.color.transparent);
            etActualStartTime.setBackgroundResource(android.R.color.transparent);
            etActualEndTime.setBackgroundResource(android.R.color.transparent);
            etActualNotes.setBackgroundResource(android.R.color.transparent);
            etNotes.setBackgroundResource(android.R.color.transparent);
            etNotes.setFocusable(false);
            etActualNotes.setFocusable(false);
        }

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (overtimeId > 0) {
                    save(baseUrl + "/api/overtimes/update/"+overtimeId+sessionUrl);
                } else {
                    save(baseUrl + "/api/overtimes" + sessionUrl);
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        if (overtimeId > 0) {
            loadData(getContext(), baseUrl+"/api/overtimes/"+overtimeId+sessionUrl);
        }


        if (role.equals("manager")) {
            if (state.equals("plan_waiting")) {
                okBtn.setText("Approve");
                okBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (overtimeId > 0) {
                            save(baseUrl + "/api/overtimes/approve_plan/" + overtimeId + sessionUrl);
                        }
                    }
                });
            }
            if (state.equals("actual_waiting")) {
                okBtn.setText("Approve");
                okBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (overtimeId > 0) {
                            save(baseUrl + "/api/overtimes/approve_actual/" + overtimeId + sessionUrl);
                        }
                    }
                });
            }
        }

        if (state.contains("plan_approved") || state.contains("actual")) {
            actualWrapper.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private void save(String url) {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put("date_plan_start", valStartDate+" "+valStartTime);
            jsonBody.put("date_plan_end", valStartDate+" "+valEndTime);
            jsonBody.put("description", etNotes.getText());

            if (valActualStartDate!= null && valActualStartTime!= null && valActualEndTime!= null) {
                jsonBody.put("date_actual_start", valActualStartDate + " " + valActualStartTime);
                jsonBody.put("date_actual_end", valActualStartDate + " " + valActualEndTime);
                jsonBody.put("description_actual", etActualNotes.getText());
            }

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url,
                jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialogInterface.onListSelected(getView(), response);
                        dismiss();
                    }
                }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String body, msg = "";
                    if (error == null || error.networkResponse == null) {
                        return;
                    }

                    //get status code here
                    final String statusCode = String.valueOf(error.networkResponse.statusCode);
                    //get response body and parse with appropriate encoding
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");
                        JSONObject jsonBody = new JSONObject(body);
                        msg = jsonBody.getString("error_descrip");
                        msg = msg.split("DETAIL")[0];
                        msg = msg.replace("('", "");
                        msg = msg.replace("', None)", "");
                    } catch (UnsupportedEncodingException e) {
                        // exception
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    dismiss();
                }
            }){

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  params = new HashMap<String, String>();
                    return params;
                }
            };
            queue.add(req);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadData(Context context, String url) {
        if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showLoading();
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).hideLoading();
                        // Display the first 500 characters of the response string.
                        try {
                            JSONObject jsonObject= new JSONObject(response);
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String datePlanStartInString = jsonObject.getString("date_plan_start");
                            String datePlanEndInString = jsonObject.getString("date_plan_end");
                            String dateActualStartInString = jsonObject.getString("date_actual_start");
                            String dateActualEndInString = jsonObject.getString("date_actual_end");
                            String timeStart = "", timeEnd = "", dateStart = "", actualTimeStart = "", actualTimeEnd = "", actualDateStart = "";
                            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
                            SimpleDateFormat timeSecondFormatter = new SimpleDateFormat("HH:mm:ss");
                            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                Date datePlanStart = formatter.parse(datePlanStartInString);
                                Date datePlanEnd = formatter.parse(datePlanEndInString);

                                etStartDate.setText(dateFormatter.format(datePlanStart));
                                etStartTime.setText(timeFormatter.format(datePlanStart));
                                etEndTime.setText(timeFormatter.format(datePlanEnd));
                                etActualStartDate.setText(dateFormatter.format(datePlanStart));
                                etActualStartTime.setText(timeFormatter.format(datePlanStart));
                                etActualEndTime.setText(timeFormatter.format(datePlanEnd));

                                valStartDate = dateFormatter.format(datePlanStart);
                                valStartTime = timeSecondFormatter.format(datePlanStart);
                                valEndTime = timeSecondFormatter.format(datePlanEnd);
                                valActualStartDate = dateFormatter.format(datePlanStart);
                                valActualStartTime = timeSecondFormatter.format(datePlanStart);
                                valActualEndTime = timeSecondFormatter.format(datePlanEnd);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            try {
                                Date dateActualStart = formatter.parse(dateActualStartInString);
                                Date dateActualEnd = formatter.parse(dateActualEndInString);

                                etActualStartDate.setText(dateFormatter.format(dateActualStart));
                                etActualStartTime.setText(timeFormatter.format(dateActualStart));
                                etActualEndTime.setText(timeFormatter.format(dateActualEnd));

                                valActualStartDate = dateFormatter.format(dateActualStart);
                                valActualStartTime = timeSecondFormatter.format(dateActualStart);
                                valActualEndTime = timeSecondFormatter.format(dateActualEnd);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            etNotes.setText(jsonObject.getString("description"));
                            etActualNotes.setText(jsonObject.getString("description_actual"));
                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = "";
                String msg = "";
                try {
                    body = new String(error.networkResponse.data,"UTF-8");
                    JSONObject jsonBody = new JSONObject(body);
                    msg = jsonBody.getString("error_descrip");
                    msg = msg.split("DETAIL")[0];
                    msg = msg.replace("('", "");
                    msg = msg.replace("', None)", "");
                } catch (UnsupportedEncodingException e) {
                    // exception
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).hideLoading();
                if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showSnackBar("Failed. " + msg);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}