package com.arkana.hris;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by root on 06/11/17.
 */

public class SelectTimeFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{

    private TimeInterface timeInterface;

    public static SelectTimeFragment newInstance(TimeInterface timeInterface) {
        SelectTimeFragment fragment = new SelectTimeFragment();
        fragment.timeInterface = timeInterface;
        return fragment;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), this, hourOfDay, minute, true);
        return timePickerDialog;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (timeInterface != null)
            timeInterface.onTimeSelected(view, hourOfDay, minute);
    }
}