package com.arkana.hris;

import android.view.View;

import org.json.JSONObject;

/**
 * Created by root on 06/10/17.
 */


public interface CRUDJSONObjectInterface {
    void onListSelected(View v, JSONObject jsonObject);
    void onListDeleted(View v, JSONObject jsonObject);
}