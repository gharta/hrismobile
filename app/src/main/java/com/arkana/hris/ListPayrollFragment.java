package com.arkana.hris;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 06/11/17.
 */

public class ListPayrollFragment extends Fragment {
    RecyclerView gRecycler;
    GAdapter gAdapter;
    CRUDJSONObjectInterface JSONObjectInterface;

    String baseUrl;
    String sessionUrl;

    String state;

    // Global Variable to Help
    JSONArray serverArray = new JSONArray();
    JSONArray offlineArray = new JSONArray();
    int globalSyncIterator = 0;


    public static ListPayrollFragment newInstance() {
        ListPayrollFragment fragment = new ListPayrollFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getActivity() != null && getActivity() instanceof MainActivity) {
            baseUrl = ((MainActivity) getActivity()).getBaseUrl();
            sessionUrl = ((MainActivity) getActivity()).getSessionUrl();
            ((MainActivity) getActivity()).setActionBarTitle("List Payroll");
        }
        if (sessionUrl.isEmpty()) {
            Activity activity = getActivity();
            Fragment fragment = LoginFragment.newInstance();
            if (activity instanceof MainActivity) ((MainActivity) activity).replaceFragment(fragment);
        }

        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_list_payroll, container, false);

        JSONObjectInterface = new CRUDJSONObjectInterface() {
            @Override
            public void onListSelected(View v, JSONObject jsonObject) {
                if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showLoading();
                try {
                    if (jsonObject != null) {
                        Integer id = jsonObject.getInt("id");
                        ((MainActivity) getActivity()).downloadWithDM(null,
                                baseUrl+"/report/pdf/hr_inj.report_payslip_ta/"+id+sessionUrl,
                                ".pdf", "application/pdf");
                    } else {
                    }
                    if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).hideLoading();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onListDeleted(View v, JSONObject jsonObject) {
                try {
                    final Integer id = jsonObject.getInt("id");
                    new AlertDialog.Builder(getActivity())
                        .setTitle("Delete Confirmation")
                        .setMessage("Do you really want to remove the data?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                            }})
                        .setNegativeButton("Cancel", null).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        // Set Adapter
        gRecycler = (RecyclerView) view.findViewById(R.id.gRecycler);
        gAdapter = new GAdapter(JSONObjectInterface);
        gRecycler.setAdapter(gAdapter);

        // Draw Layout List to View
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        gRecycler.setLayoutManager(mLayoutManager);
        gRecycler.setItemAnimator(new DefaultItemAnimator());

        gRecycler.setHasFixedSize(true);
        gRecycler.setItemViewCacheSize(20);

        // Get Data
        loadData(getContext(), baseUrl+"/api/payroll"+sessionUrl, gAdapter);
        return view;
    }


    // Adapter
    public class GAdapter extends RecyclerView.Adapter<GAdapter.ViewHolder> {

        private LayoutInflater layoutInflater;
        private JSONArray jsonArray = new JSONArray();
        CRUDJSONObjectInterface gAdapterJSONObjectInterface;

        // Interface adalah Fungsi Untuk Transfer Data dari Adapter ke Activity
        public GAdapter(CRUDJSONObjectInterface JSONObjectInterface) {
            this.gAdapterJSONObjectInterface = JSONObjectInterface;
        }
        // Inflater : Panggil Layout List
        @Override
        public GAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (layoutInflater == null)
                layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.list_payroll, parent, false);
            return new GAdapter.ViewHolder(view);
        }
        // View Holder : Definisikan Atribut di Layout List
        public class ViewHolder extends RecyclerView.ViewHolder {
            private JSONObject jsonObject;
            private TextView tvNo;
            private TextView tvCode;
            private TextView tvName;
            private TextView tvStartTime;
            private TextView tvEndTime;
            private TextView tvTotal;
            public ViewHolder(View itemView) {
                super(itemView);
                tvNo = (TextView) itemView.findViewById(R.id.tvNo);
                tvCode = (TextView) itemView.findViewById(R.id.tvCode);
                tvName = (TextView) itemView.findViewById(R.id.tvName);
                tvStartTime = (TextView) itemView.findViewById(R.id.tvStartTime);
                tvEndTime = (TextView) itemView.findViewById(R.id.tvEndTime);
                tvTotal = (TextView) itemView.findViewById(R.id.tvTotal);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gAdapterJSONObjectInterface.onListSelected(v, jsonObject);
                    }
                });
            }
            public void bindContent(JSONObject jsonObject) {
                this.jsonObject = jsonObject;
            }
        }
        // Masukkan Nilai Atribut Layout dari Objek
        @Override
        public void onBindViewHolder(GAdapter.ViewHolder holder, int position) {
            try {
                JSONObject jsonObject = getObject(position);
                holder.bindContent(jsonObject);
                Integer no = position + 1;

                holder.tvNo.setText(Integer.toString(no));
                setTextFromJSONString(holder.tvCode, jsonObject, "number");
                setTextFromJSONString(holder.tvName, jsonObject, "name");
//                setTextFromJSONObject(holder.tvName, jsonObject, "employee_id", "name");
                setTextFromJSONString(holder.tvStartTime, jsonObject, "date_from");
                setTextFromJSONString(holder.tvEndTime, jsonObject, "date_to");
                Long total = jsonObject.getLong("total");
                holder.tvTotal.setText(total.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // Iteration Tools
        private JSONObject getObject(int position) throws JSONException {
            return jsonArray.getJSONObject(position);
        }
        public void setObjectById(JSONObject jsonObject) throws JSONException {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jObj = jsonArray.getJSONObject(i);
                try {
                    if (jObj.getInt("id") == jsonObject.getInt("id"))
                        jsonArray.put(i, jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        public void setObject(int position, JSONObject jsonObject) throws JSONException {
            jsonArray.put(position, jsonObject);
        }
        public void addObject(JSONObject jsonObject) throws JSONException {
            jsonArray.put(jsonObject);
        }
        @Override
        public int getItemCount() {
            return jsonArray.length();
        }
        public void addArray(JSONArray jsonArray) {
            this.jsonArray = jsonArray;
        }
        public void clear() {
            jsonArray = new JSONArray();
        }
    }

    @SuppressLint("ResourceType")
    public static String getId(View view) {
        if (view.getId() == 0xffffffff) return "no-id";
        else {
            String full_id = view.getResources().getResourceName(view.getId());
            String[] array_id = full_id.split("/");
            String short_id = array_id[array_id.length-1];
            return short_id;
        }
    }

    private void setTextFromJSONString(TextView tv, JSONObject jsonObject, String key, String defaultValue) {
        if (jsonObject.has(key)) {
            try {
                if(jsonObject.getString(key).isEmpty()) {
                    tv.setText(defaultValue);
                } else {
                    tv.setText(jsonObject.getString(key));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setTextFromJSONString(TextView tv, JSONObject jsonObject, String key) {
        if (jsonObject.has(key)) {
            try {
                tv.setText(jsonObject.getString(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setTextFromJSONObject(TextView tv, JSONObject jsonObject, String keyObject, String keyName) {
        if (jsonObject.has(keyObject)) {
            try {
                JSONObject childObject = jsonObject.getJSONObject(keyObject);
                if (childObject != null && childObject.has(keyName)) {
                    tv.setText(childObject.getString(keyName));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setTextFromJSONObject(TextView tv, JSONObject jsonObject, String keyObject, String keyName, String defaultValue) {
        if (jsonObject.has(keyObject)) {
            try {
                JSONObject childObject = jsonObject.getJSONObject(keyObject);
                if (childObject != null && childObject.has(keyName)) {
                    if(childObject.getString(keyName).isEmpty()) {
                        tv.setText(defaultValue);
                    } else {
                        tv.setText(childObject.getString(keyName));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private JSONArray concatArray(JSONArray... arrs)
            throws JSONException {
        JSONArray result = new JSONArray();
        for (JSONArray arr : arrs) {
            for (int i = 0; i < arr.length(); i++) {
                result.put(arr.get(i));
            }
        }
        return result;
    }

    private void loadData(Context context, String url, final GAdapter adapter) {
        if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showLoading();
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).hideLoading();
                        // Display the first 500 characters of the response string.
                        try {
                            JSONArray jArr= new JSONArray(response);
                            adapter.addArray(jArr);
                            adapter.notifyDataSetChanged();
                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = "";
                String msg = "";
                try {
                    body = new String(error.networkResponse.data,"UTF-8");
                    JSONObject jsonBody = new JSONObject(body);
                    msg = jsonBody.getString("error_descrip");
                    msg = msg.split("DETAIL")[0];
                    msg = msg.replace("('", "");
                    msg = msg.replace("', None)", "");
                } catch (UnsupportedEncodingException e) {
                    // exception
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).hideLoading();
                if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showSnackBar("Failed. " + msg);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    private boolean deleteData(long id) {

        return false;
    }
}