package com.arkana.hris;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by root on 06/11/17.
 */

public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private DateInterface gDateInterface;

    public static SelectDateFragment newInstance(DateInterface gDateInterface) {
        SelectDateFragment fragment = new SelectDateFragment();
        fragment.gDateInterface = gDateInterface;
        return fragment;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
//        populateSetDate(yy, mm+1, dd);
        if (gDateInterface != null)
            gDateInterface.onDateSelected(view, yy, mm+1, dd);
    }

}