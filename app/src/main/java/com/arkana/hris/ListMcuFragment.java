package com.arkana.hris;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 06/11/17.
 */

public class ListMcuFragment extends Fragment {
    RecyclerView gRecycler;
    GAdapter gAdapter;
    CRUDJSONObjectInterface JSONObjectInterface;

    String baseUrl;
    String sessionUrl;
    String role;
    String state;

    public static ListMcuFragment newInstance() {
        ListMcuFragment fragment = new ListMcuFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getActivity() != null && getActivity() instanceof MainActivity) {
            baseUrl = ((MainActivity) getActivity()).getBaseUrl();
            sessionUrl = ((MainActivity) getActivity()).getSessionUrl();
            role = ((MainActivity) getActivity()).getRole();
            ((MainActivity) getActivity()).setActionBarTitle("List MCU");
        }
        if (sessionUrl.isEmpty()) {
            Activity activity = getActivity();
            Fragment fragment = LoginFragment.newInstance();
            if (activity instanceof MainActivity) ((MainActivity) activity).replaceFragment(fragment);
        }

        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_list_overtime, container, false);

        FloatingActionButton fabButton = view.findViewById(R.id.fab);
        FloatingActionButton exportButton = view.findViewById(R.id.export);

        JSONObjectInterface = new CRUDJSONObjectInterface() {
            @Override
            public void onListSelected(View v, JSONObject jsonObject) {
                try {
                    if (jsonObject != null) {
                        Integer id = jsonObject.getInt("id");
                        String selectedState = null;
                        if (jsonObject.has("state"))
                            selectedState = jsonObject.getJSONObject("state").getString("value");
                        Activity activity = getActivity();

                        DialogFragment fragment = DialogFormOvertimeFragment.newInstance(id, jsonObject, selectedState, new ObjectInterface() {
                            @Override
                            public void onListSelected(View v, JSONObject selected) {
                                // Get Data
                                loadData(getContext(), baseUrl+"/api/overtimes"+sessionUrl+"&state="+state+"&role="+role, gAdapter);
//                                try {
//                                    gAdapter.setObjectById(selected);
//                                    gAdapter.notifyDataSetChanged();
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
                            }
                        });
                        fragment.show(getFragmentManager(), DialogFormOvertimeFragment.class.getSimpleName());
                    } else {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onListDeleted(View v, JSONObject jsonObject) {
                try {
                    final Integer id = jsonObject.getInt("id");
                    new AlertDialog.Builder(getActivity())
                            .setTitle("Delete Confirmation")
                            .setMessage("Do you really want to remove the data?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {

                                }})
                            .setNegativeButton("Cancel", null).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        // Set Adapter
        gRecycler = (RecyclerView) view.findViewById(R.id.gRecycler);
        gAdapter = new GAdapter(JSONObjectInterface);
        gRecycler.setAdapter(gAdapter);

        // Draw Layout List to View
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        gRecycler.setLayoutManager(mLayoutManager);
        gRecycler.setItemAnimator(new DefaultItemAnimator());

        gRecycler.setHasFixedSize(true);
        gRecycler.setItemViewCacheSize(20);


        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment fragment = DialogFormOvertimeFragment.newInstance(0, null, "", new ObjectInterface() {
                    @Override
                    public void onListSelected(View v, JSONObject selected) {
                        try {
                            gAdapter.addObject(selected);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                fragment.show(getFragmentManager(), DialogFormOvertimeFragment.class.getSimpleName());
            }
        });

        exportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String employeeId = null;
                if (((MainActivity) getActivity()).getEmployeeId() != null)
                    employeeId = ((MainActivity) getActivity()).getEmployeeId().toString();
                if (employeeId != null)
                    ((MainActivity) getActivity()).downloadWithDM(null,
                            baseUrl+"/report/xlsx/hr_inj.hr_overtime_xlsx"+sessionUrl+"&employee_id="+employeeId+"&date_from=2018-09-01&date_to=2018-09-30&",
                            ".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
        });



        // Get Data
        loadData(getContext(), baseUrl+"/api/mcus"+sessionUrl, gAdapter);
        return view;
    }


    // Adapter
    public class GAdapter extends RecyclerView.Adapter<GAdapter.ViewHolder> {

        private LayoutInflater layoutInflater;
        private JSONArray jsonArray = new JSONArray();
        CRUDJSONObjectInterface gAdapterJSONObjectInterface;

        // Interface adalah Fungsi Untuk Transfer Data dari Adapter ke Activity
        public GAdapter(CRUDJSONObjectInterface JSONObjectInterface) {
            this.gAdapterJSONObjectInterface = JSONObjectInterface;
        }
        // Inflater : Panggil Layout List
        @Override
        public GAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (layoutInflater == null)
                layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.list_overtime, parent, false);
            return new GAdapter.ViewHolder(view);
        }
        // View Holder : Definisikan Atribut di Layout List
        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView tvNo;
            private TextView tvDate;
            private TextView tvCode;
            private TextView tvName;
            private TextView tvStartTime;
            private TextView tvEndTime;
            private TextView tvProject;
            private TextView tvRegion;
            private TextView tvState;
            private ImageView deleteIcon;
            private JSONObject jsonObject;
            private LinearLayout actualTimeWrapper;
            private LinearLayout approverWrapper;
            private TextView tvActualStartTime;
            private TextView tvActualEndTime;
            private TextView tvNextApprover;
            public ViewHolder(View itemView) {
                super(itemView);
                tvNo = (TextView) itemView.findViewById(R.id.tvNo);
                tvDate = (TextView) itemView.findViewById(R.id.tvDate);
                tvCode = (TextView) itemView.findViewById(R.id.tvCode);
                tvName = (TextView) itemView.findViewById(R.id.tvName);
                tvStartTime = (TextView) itemView.findViewById(R.id.tvStartTime);
                tvEndTime = (TextView) itemView.findViewById(R.id.tvEndTime);
                tvProject = (TextView) itemView.findViewById(R.id.tvProject);
                tvRegion = (TextView) itemView.findViewById(R.id.tvRegion);
                tvState = (TextView) itemView.findViewById(R.id.tvState);
                deleteIcon = (ImageView) itemView.findViewById(R.id.deleteIcon);
                actualTimeWrapper = (LinearLayout) itemView.findViewById(R.id.actualTimeWrapper);
                tvActualStartTime = (TextView) itemView.findViewById(R.id.tvActualStartTime);
                tvActualEndTime = (TextView) itemView.findViewById(R.id.tvActualEndTime);
                tvNextApprover = (TextView) itemView.findViewById(R.id.tvNextApprover);
                approverWrapper = (LinearLayout) itemView.findViewById(R.id.approverWrapper);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gAdapterJSONObjectInterface.onListSelected(v, jsonObject);
                    }
                });
                deleteIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gAdapterJSONObjectInterface.onListDeleted(v, jsonObject);
                    }
                });
            }
            public void bindContent(JSONObject jsonObject) {
                this.jsonObject = jsonObject;
            }
        }
        // Masukkan Nilai Atribut Layout dari Objek
        @Override
        public void onBindViewHolder(GAdapter.ViewHolder holder, int position) {
            try {
                JSONObject jsonObject = getObject(position);
                holder.bindContent(jsonObject);
                Integer no = position + 1;

                holder.tvNo.setText(Integer.toString(no));
                holder.tvDate.setText("asd");
                holder.tvDate.setText(jsonObject.getString("name"));

                setTextFromJSONString(holder.tvCode, jsonObject, "name");
                setTextFromJSONObject(holder.tvName, jsonObject, "employee_id", "name");
                setTextFromJSONObject(holder.tvProject, jsonObject, "project_id", "name");
                setTextFromJSONObject(holder.tvRegion, jsonObject, "project_region_id", "name");
                setTextFromJSONObject(holder.tvState, jsonObject, "state", "name");

                if (jsonObject.has("next_approval_id")) {
                    if (jsonObject.getJSONObject("next_approval_id") != null
                            && jsonObject.getJSONObject("next_approval_id").getInt("id") > 0) {
                        setTextFromJSONObject(holder.tvNextApprover, jsonObject, "next_approval_id", "name");
                        holder.approverWrapper.setVisibility(View.VISIBLE);
                    }
                }


//                String andSession = sessionUrl.replace("?", "&");
//                if (jsonObject.has("imageString")) {
//                    String imageString = jsonObject.getString("imageString");
//                    Bitmap bitmap = ((MainActivity) getActivity()).StringToBitMap(imageString);
//                    holder.imageView.setImageBitmap(bitmap);
//                } else if (jsonObject.has("image") && jsonObject.getString("image") != null && !jsonObject.getString("image").isEmpty()) {
//                    String imageURL = jsonObject.getString("image")+andSession;
//                    Picasso.with(getActivity()).invalidate(imageURL);
//                    Picasso.with(getActivity()).load(imageURL).networkPolicy(NetworkPolicy.NO_CACHE).memoryPolicy(MemoryPolicy.NO_CACHE);
//                    Picasso.with(getActivity()).load(imageURL)
//                            .fit()
//                            .centerCrop()
//                            .into(holder.imageView);
//                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // Iteration Tools
        private JSONObject getObject(int position) throws JSONException {
            return jsonArray.getJSONObject(position);
        }
        public void setObjectById(JSONObject jsonObject) throws JSONException {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jObj = jsonArray.getJSONObject(i);
                try {
                    if (jObj.getInt("id") == jsonObject.getInt("id"))
                        jsonArray.put(i, jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        public void setObject(int position, JSONObject jsonObject) throws JSONException {
            jsonArray.put(position, jsonObject);
        }
        public void addObject(JSONObject jsonObject) throws JSONException {
            jsonArray.put(jsonObject);
        }
        @Override
        public int getItemCount() {
            return jsonArray.length();
        }
        public void addArray(JSONArray jsonArray) {
            this.jsonArray = jsonArray;
        }
        public void clear() {
            jsonArray = new JSONArray();
        }
    }

    private void setTextFromJSONString(TextView tv, JSONObject jsonObject, String key, String defaultValue) {
        if (jsonObject.has(key)) {
            try {
                if(jsonObject.getString(key).isEmpty()) {
                    tv.setText(defaultValue);
                } else {
                    tv.setText(jsonObject.getString(key));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setTextFromJSONString(TextView tv, JSONObject jsonObject, String key) {
        if (jsonObject.has(key)) {
            try {
                tv.setText(jsonObject.getString(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setTextFromJSONObject(TextView tv, JSONObject jsonObject, String keyObject, String keyName) {
        if (jsonObject.has(keyObject)) {
            try {
                JSONObject childObject = jsonObject.getJSONObject(keyObject);
                if (childObject != null && childObject.has(keyName)) {
                    tv.setText(childObject.getString(keyName));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setTextFromJSONObject(TextView tv, JSONObject jsonObject, String keyObject, String keyName, String defaultValue) {
        if (jsonObject.has(keyObject)) {
            try {
                JSONObject childObject = jsonObject.getJSONObject(keyObject);
                if (childObject != null && childObject.has(keyName)) {
                    if(childObject.getString(keyName).isEmpty()) {
                        tv.setText(defaultValue);
                    } else {
                        tv.setText(childObject.getString(keyName));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private JSONArray concatArray(JSONArray... arrs)
            throws JSONException {
        JSONArray result = new JSONArray();
        for (JSONArray arr : arrs) {
            for (int i = 0; i < arr.length(); i++) {
                result.put(arr.get(i));
            }
        }
        return result;
    }

    private void loadData(Context context, String url, final GAdapter adapter) {
        if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).showLoading();
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).hideLoading();
                        // Display the first 500 characters of the response string.
                        try {
                            JSONArray jArr= new JSONArray(response);
                            adapter.addArray(jArr);
                            adapter.notifyDataSetChanged();
                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (getActivity() != null && getActivity() instanceof MainActivity) ((MainActivity) getActivity()).hideLoading();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }


    private boolean deleteData(long id) {

        return false;
    }
}