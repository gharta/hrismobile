package com.arkana.hris;

import android.view.View;

/**
 * Created by root on 06/10/17.
 */


public interface TimeInterface {
    void onTimeSelected(View v, int hour, int minute);

}