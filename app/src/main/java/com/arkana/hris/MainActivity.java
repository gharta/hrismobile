package com.arkana.hris;

import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Context context = this;
//    String baseUrl = "http://192.168.0.22:8069";
//    String baseUrl = "https://erpdev.inconis.com";
    String baseUrl = "https://erp.inconis.com";
    String sessionUrl;
    String sessionId;

    String role = "";
    String mode = "";

    CoordinatorLayout mainLayout;
    FragmentManager frgManager;
    FragmentTransaction fragmentTransaction;
    String fragmentTag;
    boolean flagExit = true;
    boolean doubleBackToExitPressedOnce = false;
    LinearLayout loadingLayout;

    DrawerLayout drawer;
    NavigationView navigationView;
    TextView tvName;
    TextView tvUsername;
    String name;
    String username;
    String payslipURL;
    String overtimeURL;
    Long employeeId;
    Toolbar toolbar;
    FrameLayout mainContentFrame;

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getPayslipURL() {
        return payslipURL;
    }

    public void setPayslipURL(String payslipURL) {
        this.payslipURL = payslipURL;
    }

    public String getOvertimeURL() {
        return overtimeURL;
    }

    public void setOvertimeURL(String overtimeURL) {
        this.overtimeURL = overtimeURL;
    }

    DownloadManager dm;
    long enqueue;
    String mimetype;
    // Progress Dialog
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainLayout = findViewById(R.id.mainCoordLayout);
        loadingLayout = findViewById(R.id.loadingLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        lockDrawer();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        tvName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tvName);
        tvUsername = (TextView) navigationView.getHeaderView(0).findViewById(R.id.tvUsername);
        mainContentFrame = (FrameLayout) findViewById(R.id.main_content_frame);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        // AUTOMATIC LOGIN
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        Fragment fragment = null;
        if (prefs.contains("SESSION_ID")) {
            sessionUrl = "?session_id="+prefs.getString("SESSION_ID", "");
            sessionId = prefs.getString("SESSION_ID", "");
            fragment = ListOvertimeFragment.newInstance("plan_draft,plan_waiting");
            unlockDrawer();
        } else {
            sessionUrl = "?session_id=";
            sessionId = "";
            fragment = LoginFragment.newInstance();
        }
        if (prefs.contains("role")) {
            setRole(prefs.getString("role", ""));
            setNavigation(prefs.getString("role", ""));
        }
        if (prefs.contains("employee_id"))
            setEmployeeId(prefs.getLong("employee_id", 0));
        if (prefs.contains("name"))
            setName(prefs.getString("name", ""));
        if (prefs.contains("username"))
            setUsername(prefs.getString("username", ""));
        if (prefs.contains("payslip_url"))
            setPayslipURL(prefs.getString("payslip_url", ""));
        if (prefs.contains("overtime_url"))
            setOvertimeURL(prefs.getString("overtime_url", ""));

        frgManager= getSupportFragmentManager();
        fragmentTransaction = frgManager.beginTransaction();
        fragmentTransaction.setAllowOptimization(false);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.add(R.id.main_content_frame, fragment).commit();
        frgManager.executePendingTransactions();

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        100);
            }
        }
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        200);
            }
        }

        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    try {
                        long downloadId = intent.getLongExtra(
                                DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                        DownloadManager.Query query = new DownloadManager.Query();
                        query.setFilterById(enqueue);
                        Cursor c = dm.query(query);
                        if (c.moveToFirst()) {
                            int columnIndex = c
                                    .getColumnIndex(DownloadManager.COLUMN_STATUS);
                            if (DownloadManager.STATUS_SUCCESSFUL == c
                                    .getInt(columnIndex)) {
                                String uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                                Intent newIntent = new Intent(Intent.ACTION_VIEW);
                                newIntent.setDataAndType(Uri.parse(uriString), mimetype);
                                try {
                                    startActivity(newIntent);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(context, "No handler for this type of file.", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    } catch (Throwable t) {
                    }
                }
            }
        };

        registerReceiver(receiver, new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE));


        // Check Connection
        final Handler ha=new Handler();
        ha.postDelayed(new Runnable() {
            @Override
            public void run() {
                //call function
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                if (prefs.contains("SESSION_ID")) {
                    String dateString = prefs.getString("notification_date", "1970-01-01 00:00:00");
                    checkNotification(getBaseContext(), baseUrl + "/api/overtimes/notif" + sessionUrl + "&create_date=" + dateString);
                }

                ha.postDelayed(this, 15000);
            }
        }, 15000);
    }

    private void createHandler() {
        Thread thread = new Thread() {
            public void run() {
                Looper.prepare();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // Do Work
                        handler.removeCallbacks(this);
                        Looper.myLooper().quit();
                    }
                }, 5000);

                Looper.loop();
            }
        };
        thread.start();
    }

    private void checkNotification(Context context, String url) {
        showLoading();
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(context);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideLoading();
                        // Display the first 500 characters of the response string.
                        try {
                            JSONArray jArr= new JSONArray(response);
                            for (int j = 0; j < jArr.length(); j++) {
                                JSONObject jsonObject = jArr.getJSONObject(j);
                                String MESSAGE_TAG = "Notification";
                                String title = getString(R.string.app_name);

                                String message = jsonObject.getString("message");
                                Intent i = new Intent(MESSAGE_TAG);
                                i.putExtra(MESSAGE_TAG, message);
                                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(i);

                                int notificationId = new Random().nextInt(60000);
                                Intent intent = new Intent(getBaseContext(), ListOvertimeFragment.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, intent, 0);
                                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getBaseContext(), "ADMIN_CHANNEL_ID")
                                        .setSmallIcon(R.drawable.inconis) //a resource for your custom small icon
                                        .setContentTitle(title) //the "title" value you sent in your notification
                                        .setContentText(message) //ditto
                                        .setAutoCancel(true) //dismisses the notification on click
                                        .setContentIntent(pendingIntent)
                                        .setPriority(Notification.PRIORITY_MAX)
                                        .setVibrate(new long[] { 0, 200, 300, 200, 300 })
                                        .setLights(Color.BLUE, 3000, 3000)
                                        ;
                                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());

                                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                formatter.setTimeZone(TimeZone.getTimeZone("gmt"));
                                String dateString = formatter.format(new Date());
                                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("notification_date", dateString);
                                editor.commit();
                            }
                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String body = "";
                String msg = "";
                try {
                    body = new String(error.networkResponse.data,"UTF-8");
                    JSONObject jsonBody = new JSONObject(body);
                    msg = jsonBody.getString("error_descrip");
                    msg = msg.split("DETAIL")[0];
                    msg = msg.replace("('", "");
                    msg = msg.replace("', None)", "");
                } catch (UnsupportedEncodingException e) {
                    // exception
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                hideLoading();
//                showSnackBar("Failed. " + msg);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String>  params = new HashMap<String, String>();
                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void downloadWithDM(View view, String url, String extension, String mimetype) {
        dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(
                Uri.parse(url));
        String nameOfFile = URLUtil.guessFileName(url, null,
                MimeTypeMap.getFileExtensionFromUrl(url));
        if (extension != null)
            nameOfFile = nameOfFile + extension;
        request.setDescription(nameOfFile);
        request.setTitle(nameOfFile);
        request.setMimeType(mimetype);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
 /*here the name of file is set*/nameOfFile);
        enqueue = dm.enqueue(request);
        this.mimetype = mimetype;

    }

    public void lockDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public void unlockDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public void showToolbar() {
        toolbar.setVisibility(View.VISIBLE);
    }

    public void hideToolbar() {
        toolbar.setVisibility(View.GONE);
    }

    public void showDownload(View view) {
        Intent i = new Intent();
        i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
        startActivity(i);
    }

    private String fileExt(String url) {
        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }
        if (url.lastIndexOf(".") == -1) {
            return null;
        } else {
            String ext = url.substring(url.lastIndexOf(".") + 1);
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();
        }
    }

//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_plan_waiting) {
            Fragment fragment = ListOvertimeFragment.newInstance("plan_draft,plan_waiting");
            replaceFragment(fragment);
            mode = "plan_request";
        } else if (id == R.id.nav_plan_approved) {
            Fragment fragment = ListOvertimeFragment.newInstance("plan_waiting");
            replaceFragment(fragment);
            mode = "plan_approval";
        } else if (id == R.id.nav_actual_waiting) {
            Fragment fragment = ListOvertimeFragment.newInstance("plan_approved,actual_waiting");
            replaceFragment(fragment);
            mode = "actual_request";
        } else if (id == R.id.nav_actual_approved) {
            Fragment fragment = ListOvertimeFragment.newInstance("actual_waiting");
            replaceFragment(fragment);
            mode = "actual_approval";
        } else if (id == R.id.nav_history) {
            Fragment fragment = ListOvertimeFragment.newInstance("actual_approved,cancel");
            replaceFragment(fragment);
            mode = "report";
        } else if (id == R.id.nav_payslip) {
            Fragment fragment = ListPayrollFragment.newInstance();
            replaceFragment(fragment);
            mode = "report";
        } else if (id == R.id.nav_mcu) {
            Fragment fragment = ListMcuFragment.newInstance();
            replaceFragment(fragment);
            mode = "report";
        }else if (id == R.id.nav_logout) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = prefs.edit();
            Fragment fragment = LoginFragment.newInstance();
            confirmReplaceFragment(fragment, "Logout Confirmation", "Do you want to log out?");
            editor.remove("SESSION_ID");
            editor.remove("role");
            editor.remove("name");
            editor.remove("employee_id");
            editor.remove("username");
            editor.remove("payslip_url");
            editor.remove("overtime_url");
            editor.remove("LAST_PULL");
            editor.commit();
            lockDrawer();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setName(String res) {
        name = res;
        tvName.setText(res);
    }

    public void setUsername(String res) {
        username = res;
        tvUsername.setText(res);
    }


    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getSessionUrl() {
        return sessionUrl;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public void setSessionUrl(String sesionUrl) {
        this.sessionUrl = sesionUrl;
    }

    public void showLoading() {
        loadingLayout.setVisibility(LinearLayout.VISIBLE);
    }

    public void hideLoading() {
        loadingLayout.setVisibility(LinearLayout.GONE);
    }

    public void showSnackBar(String msg) {
        Snackbar snackbar = Snackbar.make(mainLayout, msg, Snackbar.LENGTH_LONG)
                .setActionTextColor(ContextCompat.getColor(this, R.color.colorAccent));
//                .setAction("ACTION", new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                });
        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

//    Replace Fragment
//    public void confirmReplaceFragment(final Fragment fragment) {
//        // If previous fargment is Form Fragment
//        Fragment prevFragment = getSupportFragmentManager().findFragmentByTag("FORM");
//        if (prevFragment != null && prevFragment.getClass().equals(FormFragment.class) && prevFragment.isVisible()) {
//            new AlertDialog.Builder(this)
//                    .setTitle("Leave Confirmation")
//                    .setMessage("Do you really want to leave ? The data may be permanently discarded")
//                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int whichButton) {
//                            replaceFragment(fragment);
//                        }})
//                    .setNegativeButton("Cancel", null).show();
//        } else {
//            replaceFragment(fragment);
//        }
//    }

    public void confirmReplaceFragment(final Fragment fragment, String title, String msg) {
        // If previous fargment is Form Fragment
        new AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(msg)
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    replaceFragment(fragment);
                }})
            .setNegativeButton("Cancel", null).show();
    }
    public void replaceFragment(final Fragment fragment) {
        frgManager= getSupportFragmentManager();
        fragmentTransaction = frgManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.main_content_frame, fragment, fragmentTag);
        fragmentTransaction.commit();
    }

    public void setNavigation(String role) {
        if (role.equals("manager")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_main_drawer_manager);
        } else {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_main_drawer);
        }
    }

    // For Dialog
    public void addFragment(Fragment fragment) {
        frgManager= getSupportFragmentManager();
        fragmentTransaction = frgManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.add(R.id.main_content_frame, fragment, fragmentTag);
        fragmentTransaction.commit();
    }

    public void reloadFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.detach(fragment);
        ft.attach(fragment);
        ft.commit();
    }

    public void clearBackStack() {
        for(int i = 0; i < frgManager.getBackStackEntryCount(); ++i) {
            frgManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }


    public void setActionBarTitle(String title) {
        MainActivity instance = this;
        if (instance != null) {
            SpannableString s = new SpannableString(title);
            s.setSpan(new TypefaceSpan("sans-serif-light"), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            instance.setTitle(s);
        } else {
        }
    }



    /**
     * Showing Dialog
     * */

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    public void download(String file_url) {
        new DownloadFileFromURL().execute(file_url);
    }

    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()+"/"+"Name.xlsx");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);

        }

    }

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    @Override
    public void onBackPressed()
    {
        if (flagExit) {
            if (doubleBackToExitPressedOnce) {
                doubleBackToExitPressedOnce = false;
                finish();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (doubleBackToExitPressedOnce)
                        if (frgManager.getBackStackEntryCount() > 1)
                            frgManager.popBackStackImmediate();
                    doubleBackToExitPressedOnce = false;
                }
            }, 1000);
        } else {
            super.onBackPressed();  // optional depending on your needs
        }
    }
}
